FROM python:3.7-slim

# hadolint ignore=DL3008,DL3013
RUN apt-get --quiet --quiet update \
    && apt-get --yes --quiet --quiet install --no-install-recommends \
        git \
    && git clone --depth 1 \
        https://github.com/x0rz/tweets_analyzer /opt/tweets_analyzer \
    && apt-get --yes --quiet --quiet purge --auto-remove git \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/* \
    \
    && \
    pip --disable-pip-version-check --no-cache-dir install --upgrade \
        pip \
        setuptools \
        wheel \
    pip --disable-pip-version-check --no-cache-dir install -r /opt/tweets_analyzer/requirements.txt \
    \
    && \
    ln -sf /secrets.py /opt/tweets_analyzer/secrets.py

ENTRYPOINT [ "/opt/tweets_analyzer/tweets_analyzer.py" ]
CMD [ "--help" ]
